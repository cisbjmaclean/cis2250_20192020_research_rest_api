package com.example.test_rest;

import retrofit2.Call;
import java.util.List;
import retrofit2.http.GET;

public interface JsonPlaceHolderApi {
    @GET("posts")
    Call<List<Post>> getPosts();

    @GET("user")
    Call<List<User>> getUsers();

    @GET("member")
    Call<List<Member>> getMembers();
}
